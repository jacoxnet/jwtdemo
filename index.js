const express = require('express');
const app = express();
const PORT = 5000;
const jwt = require('jsonwebtoken');

const secret = 'mysecretphrase';
const expiry = 36000;

app.use(express.json());

// create a token and send back to client
app.post('/sign-token', (req, res) => {
    // create payload
    const payload = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        id: req.body.id
    };
    if (!payload.firstName || !payload.lastName || !payload.id)
        return res.status(400).json({message: 'Must include firstName, lastName, id'});
    // sign 
    jwt.sign(payload, secret, {expiresIn: expiry}, (err, token) => {
        if (!err) return res.status(200).json({token});
        return res.status(500).json({err});
    });
});


// receive a token from client and decode
app.get('/decode-token', (req, res) => {
    // retrieve authorization header, extract token, decode
    const authHeader = req.headers.authorization;
    if (!authHeader) return res.status(403).json({message: 'authentication token required'});
    const token = authHeader.split(' ')[1];
    jwt.verify(token, secret, (err, decoded) => {
        if (err) return res.status(500).json({err});
        return res.status(200).json({user: decoded});
    });
});

// start server on PORT
app.listen(PORT, () => console.log('app listening at port ' + PORT));